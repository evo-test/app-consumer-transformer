### App Consumer Transformer
[![pipeline status](https://gitlab.com/evo-test/app-consumer-transformer/badges/master/pipeline.svg)](https://gitlab.com/evo-test/app-consumer-transformer/-/commits/master)

Simple microservice based on Moleculer, that consumes msg's containing unix timestamp from Kafka topic 'input', transforms to date string (RFC 3339) and sends to topic 'output'

<a href="https://gitlab.com/evo-test/deployments/-/blob/master/README.md">Setup instructions</a>