"use strict";

const { ServiceBroker } = require('moleculer');
const {KafkaClient, HighLevelProducer, Consumer} = require('kafka-node');
const PrometheusService = require('moleculer-prometheus');

const KAFKA_HOST = process.env.KAFKA_HOST || 'kafka.vagrant:9092';
const KAFKA_TOPIC = process.env.KAFKA_TOPIC || 'input';
const KAFKA_TRANSFORMER_TOPIC = process.env.KAFKA_TOPIC || 'output';
const PROM_PORT = process.env.PROM_PORT || 3030;

const broker = new ServiceBroker({
    logger: {
        type: 'Console',
        options: {
            level: 'info',
            colors: false,
            moduleColors: false,
            formatter: 'json',
            objectPrinter: null,
            autoPadding: false
        }
    }
});

broker.createService({
    mixins: [PrometheusService],
    settings: {
        port: PROM_PORT,
        collectDefaultMetrics: true,
        metrics: {
            'transformer_input': { type: 'Counter', help: 'Transformer input topic, requests rate' },
            'transformer_output': { type: 'Counter', help: 'Transformer output topic, requests rate' }
        }
    }
});

broker.createService({
    name: 'transformer',
    settings: {
        kafka: {
            host: KAFKA_HOST,
            consumerTopic: KAFKA_TOPIC,
            transformerTopic: KAFKA_TRANSFORMER_TOPIC
        }
    },
    actions: {},
    methods: {
        async init() {
            const kafkaClient = await this.createClient();
            this.logger.info('Connected to kafka broker');

            const kafkaProducer = new HighLevelProducer(kafkaClient, {
                requireAcks: 0
            });

            await this.createTopics(
                kafkaProducer,
                [
                    this.settings.kafka.consumerTopic,
                    this.settings.kafka.transformerTopic
                ]
            );
            this.logger.info('Topics created');

            const consumer = new Consumer(
                kafkaClient,
                [{
                    topic: this.settings.kafka.consumerTopic
                }],
                {
                    autoCommit: true
                }
            );

            this.logger.info('Consuming messages');
            consumer.on('message', (message) => {
                broker.broadcast('metrics.update', {
                    name: 'transformer_input',
                    method: 'inc',
                    value: 1
                });

                this.logger.info('Message received @ ' + this.settings.kafka.consumerTopic, message.value);
                const payload = new Date(parseInt(message.value)).toISOString();

                this.sendMessage(kafkaProducer, [{
                    topic: this.settings.kafka.transformerTopic,
                    messages: payload
                }]).then(() => {
                    this.logger.info('Message successfully sent @ ' + this.settings.kafka.transformerTopic, payload);
                }).catch((e) => {
                    this.logger.error('Send message error', e);
                });
            });

            return new Promise(() => {});
        },

        createClient() {
            this.logger.info('Connecting...');
            return new Promise((resolve, reject) => {
                const kafkaClient = new KafkaClient({
                    'kafkaHost': this.settings.kafka.host
                });

                kafkaClient.on('ready', () => resolve(kafkaClient));
                kafkaClient.on('error', (e) => reject(e));
            });
        },
        createTopics(kafkaProducer, topics) {
            return new Promise((resolve, reject) => {
                kafkaProducer.createTopics(topics, true, (err, success) => {
                    if (err) return reject(err);
                    resolve(success);
                });
            });
        },
        sendMessage(kafkaProducer, payload) {
            broker.broadcast('metrics.update', {
                name: 'transformer_output',
                method: 'inc',
                value: 1
            });

            return new Promise((resolve, reject) => {
                kafkaProducer.send(payload, (err, success) => {
                    if (err) return reject(err);
                    resolve(success);
                });
            });
        }
    },

    started() {
        this.init();
    }
});

broker.start().then(() => {
    broker.repl();
});